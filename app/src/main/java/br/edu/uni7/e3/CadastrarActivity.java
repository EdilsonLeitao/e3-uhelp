package br.edu.uni7.e3;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.File;

import br.edu.uni7.e3.dao.DoacaoDAO;
import br.edu.uni7.e3.model.Doacao;

import static android.os.Environment.DIRECTORY_PICTURES;

public class CadastrarActivity extends AppCompatActivity {

    public static final int CODIGO_CAMERA = 657;
    public CadastrarHelper cadHelper;
    private String caminhoFoto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cadastrar);
        this.setTitle("Cadastrar Doação");

        ImageView img = findViewById(R.id.img);
        img.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intentImg = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                caminhoFoto = getExternalFilesDir(DIRECTORY_PICTURES) + "/" + System.currentTimeMillis()+ ".jpg";
                File file = new File(caminhoFoto);
                intentImg.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(file));
                startActivityForResult(intentImg, CODIGO_CAMERA);
            }
        });

        cadHelper = new CadastrarHelper(this);

        Button salvar = findViewById(R.id.salvar);
        salvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg = null;

                Doacao doacao = cadHelper.getDoacao();
                String validacao = cadHelper.validaDoacao(doacao);

                if (validacao == null) {
                    DoacaoDAO dao = new DoacaoDAO(CadastrarActivity.this);
                    dao.insere(doacao);
                    Toast.makeText(CadastrarActivity.this, "Doação cadastrada", Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    Toast.makeText(CadastrarActivity.this, "Preencha o campo " + validacao + " para salvar a doação", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == Activity.RESULT_OK) {
            if (requestCode == CODIGO_CAMERA) {
                ImageView img = findViewById(R.id.img);
                Bitmap bitmap = BitmapFactory.decodeFile(caminhoFoto);
                Bitmap bitmapReduzido = Bitmap.createScaledBitmap(bitmap, 300, 300, true);
                img.setImageBitmap(bitmapReduzido);
                img.setScaleType(ImageView.ScaleType.FIT_XY);
                img.setTag(caminhoFoto);
            }
        }
    }
}
