package br.edu.uni7.e3;

import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.List;

import br.edu.uni7.e3.dao.DoacaoDAO;
import br.edu.uni7.e3.model.Doacao;

public class MapaFragment extends SupportMapFragment implements OnMapReadyCallback {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        LatLng uni7 = new LatLng(-3.771006, -38.483682);
        if (uni7 != null) {
            CameraUpdate update = CameraUpdateFactory.newLatLngZoom(uni7, 17);
            googleMap.moveCamera(update);
        }

        DoacaoDAO doacaoDAO = new DoacaoDAO(getContext());
        for (Doacao doacao : doacaoDAO.buscaDoacoes()) {
            LatLng coordenada = pegaCoordenadaDoEndereco(doacao.getEndereco());
            if (coordenada != null) {
                MarkerOptions marcador = new MarkerOptions();
                marcador.position(coordenada);
                marcador.title(doacao.getTitulo());
                marcador.snippet(String.valueOf(doacao.getDescricao()));
                googleMap.addMarker(marcador);
            }
        }
    }

    private LatLng pegaCoordenadaDoEndereco(String endereco) {
        try {
            Geocoder geocoder = new Geocoder(getContext());
            List<Address> resultados =
                    geocoder.getFromLocationName(endereco, 1);
            if (!resultados.isEmpty()) {
                LatLng posicao = new LatLng(resultados.get(0).getLatitude(), resultados.get(0).getLongitude());
                return posicao;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}