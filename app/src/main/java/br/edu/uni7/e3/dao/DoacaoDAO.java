package br.edu.uni7.e3.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import br.edu.uni7.e3.model.Doacao;

public class DoacaoDAO extends SQLiteOpenHelper {

    private SQLiteDatabase db;

    public DoacaoDAO(Context context) {
        super(context, "uHelp", null, 3);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "CREATE TABLE Doacao ( id INTEGER PRIMARY KEY, categoria TEXT NOT NULL, tipo TEXT NOT NULL, titulo TEXT NOT NULL, descricao TEXT NOT NULL, endereco TEXT NOT NULL," +
                " latitude DOUBLE, longitude DOUBLE, telefone TEXT NOT NULL, foto TEXT)";

        db.execSQL(sql);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String sql = "DROP TABLE IF EXISTS Doacao";
        db.execSQL(sql);
        this.onCreate(db);
    }

    public void insere(Doacao doacao) {

        ContentValues dados = pegaDadosDoacao(doacao);

        getWritableDatabase().insert("Doacao", null, dados);

        this.close();
    }

    public void limpaApp(){
        String sql = "DELETE FROM Doacao";
        getWritableDatabase().execSQL(sql);
        this.close();
    }

    private ContentValues pegaDadosDoacao(Doacao doacao) {
        ContentValues dados = new ContentValues();
        dados.put("categoria", doacao.getCategoria());
        dados.put("tipo", doacao.getTipo());
        dados.put("titulo", doacao.getTitulo());
        dados.put("descricao", doacao.getDescricao());
        dados.put("endereco", doacao.getEndereco());
        dados.put("telefone", doacao.getTelefone());
        dados.put("foto", doacao.getFoto());

        return dados;
    }

    public List<Doacao> buscaDoacoes() {
        String sql = "SELECT * FROM Doacao;";
        Cursor c = getReadableDatabase().rawQuery(sql, null);

        List<Doacao> doacoes = new ArrayList<Doacao>();
        while (c.moveToNext()) {
            Doacao doacao = new Doacao();
            doacao.setId(c.getLong(c.getColumnIndex("id")));
            doacao.setCategoria(c.getInt(c.getColumnIndex("categoria")));
            doacao.setTipo(c.getInt(c.getColumnIndex("tipo")));
            doacao.setTitulo(c.getString(c.getColumnIndex("titulo")));
            doacao.setDescricao(c.getString(c.getColumnIndex("descricao")));
            doacao.setEndereco(c.getString(c.getColumnIndex("endereco")));
            doacao.setTelefone(c.getString(c.getColumnIndex("telefone")));
            doacao.setFoto(c.getString(c.getColumnIndex("foto")));

            doacoes.add(doacao);
        }
        c.close();
        this.close();

        return doacoes;
    }
}
