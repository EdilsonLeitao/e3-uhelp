package br.edu.uni7.e3;

import android.text.TextUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import br.edu.uni7.e3.model.Doacao;

public class CadastrarHelper {

    private Doacao doacao;
    private final Spinner categoria;
    private final Spinner tipo;
    private final EditText titulo;
    private final EditText descricao;
    private final EditText endereco;
    private final EditText telefone;
    private final ImageView foto;

    public CadastrarHelper(CadastrarActivity act) {
        categoria = act.findViewById(R.id.categoria);
        tipo = act.findViewById(R.id.tipo);
        titulo = act.findViewById(R.id.titulo);
        descricao = act.findViewById(R.id.descricao);
        endereco = act.findViewById(R.id.endereco);
        telefone = act.findViewById(R.id.telefone);
        foto = act.findViewById(R.id.img);

        doacao = new Doacao();
    }

    public Doacao getDoacao() {
        doacao.setCategoria(categoria.getSelectedItemPosition());
        doacao.setTipo(tipo.getSelectedItemPosition());
        doacao.setTitulo(titulo.getText().toString());
        doacao.setDescricao(descricao.getText().toString());
        doacao.setEndereco(endereco.getText().toString());
        doacao.setTelefone(telefone.getText().toString());
        doacao.setFoto((String) foto.getTag());

        return doacao;
    }

    public String validaDoacao(Doacao doacao){
        String campoInvalido = null;

        if(doacao.getCategoria() == 0){
            campoInvalido = "categoria";
        } else if(doacao.getTipo() == 0){
            campoInvalido = "tipo";
        } else if(isEmpty(doacao.getTitulo())){
            campoInvalido = "titulo";
        } else if(isEmpty(doacao.getDescricao())){
            campoInvalido = "descrição";
        } else if(isEmpty(doacao.getEndereco())){
            campoInvalido = "endereço";
        } else if(isEmpty(doacao.getTelefone())){
            campoInvalido = "telefone";
        }

        return campoInvalido;
    }

    public boolean isEmpty(String str){
        return TextUtils.isEmpty(str);
    }

    public void preencheFormulario(Doacao doacao) {
        categoria.setSelection(doacao.getCategoria());
        tipo.setSelection(doacao.getTipo());
        titulo.setText(doacao.getTitulo());
        descricao.setText(doacao.getDescricao());
        endereco.setText(doacao.getEndereco());
        telefone.setText(doacao.getTelefone());

        this.doacao = doacao;
    }
}
